#!/usr/bin/env python3

import os
import re
import json
import requests
import secrets
import string
import socket
import time
import logging

from jinja2 import Environment, FileSystemLoader, TemplateNotFound

from http import HTTPStatus
import tornado.ioloop
from tornado.web import Application, RequestHandler, RedirectHandler

JUPYTER_API_URL = os.environ.get('JUPYTERHUB_API_URL')
JUPYTER_API_TOKEN = os.environ.get('JUPYTERHUB_API_TOKEN')
JUPYTER_USERNAME = os.environ.get('JUPYTERHUB_USER')
JUPYTERHUB_SERVICE_PREFIX = os.environ.get('JUPYTERHUB_SERVICE_PREFIX')

CHISEL_AUTHFILE = os.environ.get('CHISEL_AUTHFILE')
CHISEL_USERNAME = 'jovyan'
CHISEL_PASSWORD = ''.join(secrets.choice(string.ascii_letters + string.digits) for i in range(20))

state = 'init'

def chisel_get_fingerprint():
    with open('/chisel.log', 'r') as f:
        content = f.read()

        m = re.search(r'Fingerprint ([a-zA-Z0-9=+/]+)', content)
        if m:
            return m.group(1)


class TemplateRendering:

    def render_template(self, template_name, **kwargs):
        template_dirs = [
            '/'
        ]
        env = Environment(loader=FileSystemLoader(template_dirs))

        try:
            template = env.get_template(template_name)
        except TemplateNotFound:
            raise TemplateNotFound(template_name)
        return template.render(kwargs)

class ErrorHandler(RequestHandler, TemplateRendering):

    def get(self):
        self.set_status(400, 'Singleuser not connected')

class MainHandler(RequestHandler, TemplateRendering):

    def get(self):

        args = {
            'hpc_username': 'your_hpc_username',
            'prefix': JUPYTERHUB_SERVICE_PREFIX
        }

        response = self.render_template('template.html.j2', **args)

        self.write(response)

class APIHandler(RequestHandler):

    def post(self):
        self.set_header('Connection', 'close')
        self.request.connection.no_keep_alive = True

        token_hdr = self.request.headers.get('Authorization')
        token = token_hdr.split()[1]
        if not token:
            self.send_error(HTTPStatus.BAD_REQUEST)
            return

        # Check token
        r = requests.get(JUPYTER_API_URL+'/user', headers={
            'Authorization': 'token ' + token
        })
        if r.status_code != 200:
            self.send_error(r.status_code)
            return

        try:
            should_stop = self.get_argument('stop')
            if should_stop:
                self.set_header('Connection', 'close')
                self.request.connection.no_keep_alive = True
                
                global state
                state = 'release'
        except:
            pass

        response = {
            'jupyter': {
                'token': JUPYTER_API_TOKEN
            },
            'chisel': {
                'username': CHISEL_USERNAME,
                'password': CHISEL_PASSWORD,
                'fingerprint': chisel_get_fingerprint()
            }
        }

        self.write(response)


def main():
    global state

    FORMAT = '%(asctime)-15s %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)

    prefix = os.environ['JUPYTERHUB_SERVICE_PREFIX']

    # Generate Chisel authentication details
    with open(CHISEL_AUTHFILE + '.new', 'w') as f:
        auth = {
            f'{CHISEL_USERNAME}:{CHISEL_PASSWORD}': ['']
        }
        json.dump(auth, f)

    os.replace(CHISEL_AUTHFILE + '.new', CHISEL_AUTHFILE)

    ioloop = tornado.ioloop.IOLoop.current()

    def check_interruption():
        if state != 'running':
            logging.info('Stopping IO loop')
            ioloop.stop()

    tornado.ioloop.PeriodicCallback(check_interruption, 100).start()

    while True:
        if state == 'release':
            logging.info('Released port. Start retrying in 10 sec..')
            time.sleep(10)

            state = 'released'

        elif state == 'released':
            sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
            result = sock.connect_ex(('::1', 8890))
            sock.close()

            if result == 0: # Port is open
                logging.info('Port is occupied. Trying again in 1 sec..')
                time.sleep(1)
            else:
                logging.info('Port is free. Starting..')
                state = 'init'

        elif state == 'init':
            logging.info('Starting IO loop')
            state = 'running'

            app = Application([
                (prefix, MainHandler),
                (prefix+'api', ErrorHandler),
                (prefix+'api/v1', APIHandler),
                (prefix+'.+', RedirectHandler, {'url': prefix})
            ])

            server = app.listen(8890)

            ioloop.start()

            server.stop()

        elif state == 'terminating':
            break


if __name__ == '__main__':
    main()
